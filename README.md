Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?
	- logger.log is being sent to a file while the other is being sent to the console.
2.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
	- FINER indicates a fairly detailed tracing message.
3.  What does Assertions.assertThrows do?
	- Accepts a custom failure message as a String or Supplier<String>.
	- Uses canonical names for exception types when generating assertion failure messages.
	- Returns the thrown exception.
4.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
	- The serialVersionUID is used as a version control in a Serializable class. If you do not explictly
	declare a serialVersionUID, JVM will do it for you automatically, based on various aspects of your 
	Serializable class, as describled in the Java Object Serialization Specification.
	- The serialization runtime associates with each serializable class a version number, called a serialVersionUID,
	which is used during deserialization to verify that the sender and receiver of a serialized object have loaded classes 
	for that object that are compatible with respect to serialization. If the reciever has loaded a class for the object
	that has a different serialVersionUID than that of the corresponding sender's class, then deserialization will result in an
	InvalidClassException. A serializable class can declare its own serialVersionUID explicitly by declaring a field name
	"SerialVersionUID" that must be static, final, and of type long 
    2.  Why do we need to override constructors?
	- The default is highly sensitive to class details that may vary depending on compiler implementations.
    3.  Why we did not override other Exception methods?
	- Extensions of Exception are not allowed to override a method.	
5.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
	- Creates an InputStream called configFile.
	- File is set to logger.properties
	- The logManager will read from configFile
	- If the exception IOException ex is thrown two Warning messages are printed
	- Logger.info will now forward the given message to all registered output Handler objects.
6.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
	- An MD file is a text file created using one of several possible dialects pf the Markdown language. It is saved in plain text format but includes
	inline text symbols that define how to format the text(e.g., bold, indentations, headers, table formatting). MD files are designed for authoring 
	plain text documentation that can be easily converted to HTML.
	- Bitbucket Server uses Markdown for formatting text, as specified in CommonMark (with a few extensions).
7.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
	- timeToWait was less than 0 (-1). Meaning timeNow is still null by the time final is reached.
	- An unexpected exception is also being thrown NullPointerException.
	- Have another catch statement that will check for NullPointerexception.
	- timeNow has to be initialized before being used at the end.
	- timeNow was initialized to zero at the begining instead of null
8.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
	- Unexpected exception type thrown ==> expected: <edu.baylor.ecs.si.TimerException> but was: <java.lang.NullPointerException>
9.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
	- picture in a pdf titled Q_9_10
10.  Make a printScreen of your eclipse Maven test run, with console
	- picture in a pdf titled Q_9_10
11.  What category of Exceptions is TimerException and what is NullPointerException
	- NullPointerException is a RuntimeException
	- TimerException is a DecisionException
12.  Push the updated/fixed source code to your own repository.